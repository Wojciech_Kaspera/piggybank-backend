To run the server:

npm i
cd public
node server.js

================================================================================

- new accounts have to be added manually

- data from Firebase

- JWT - based authorisation

- password for each account is "password"

================================================================================

Use postman to test the endpoints:

    /accounts
    get all the existing Piggybank accounts

    /account/:id
    get specific Piggybank account

    /login
    log in - authorisation token is returned in response body

    /hash
    just for the sake of hashing the passwords - nothing important

    /check
    checking is the token still valid - should be used with token in request body, should be run every time user is trying to get some resource from app

    /ob/account/:accountId
    returns mock account - but in the exactly the same form as Open Banking API does

    /ob/account/:accountId/balances
    returns mock account balances - but in the exactly the same form as Open Banking API does

================================================================================

Accounts in the database:

[
    null,
    {
        "accountName": "RandomParent1",
        "childId": 2,
        "id": 1,
        "password": "$2a$08$8YZwhx8odFo0wCMK8bExtOWEBbtXTC5hid72co8QWWSmtb5.D2L1e",
        "type": "admin"
    },
    {
        "accountName": "RandomChild1",
        "id": 2,
        "parentId": 1,
        "password": "$2a$08$8YZwhx8odFo0wCMK8bExtOWEBbtXTC5hid72co8QWWSmtb5.D2L1e",
        "permissions": {
            "pay": {
                "limit": 20
            },
            "transfer": {
                "limit": 20
            }
        },
        "type": "child"
    },
    {
        "accountName": "RandomParent2",
        "childId": 4,
        "id": 3,
        "password": "$2a$08$8YZwhx8odFo0wCMK8bExtOWEBbtXTC5hid72co8QWWSmtb5.D2L1e",
        "type": "admin"
    },
    {
        "accountName": "RandomChild2",
        "id": 4,
        "parentId": 3,
        "password": "$2a$08$8YZwhx8odFo0wCMK8bExtOWEBbtXTC5hid72co8QWWSmtb5.D2L1e",
        "permissions": {
            "pay": {
                "limit": 10
            }
        },
        "type": "child"
    }
]