const Token = require('./token.js');
const db = require('./db');

module.exports = function (app) {

    //get all the existing Piggybank accounts
    app.get('/accounts', getAccounts);

    //get specific Piggybank account
    app.get('/account/:id', getAccount);

    //log in - authorisation token is returned in response body
    app.put('/login', login);

    //just for the sake of hashing the passwords - nothing important
    app.put('/hash', hash);

    //checking is the token still valid - should be used with token in request body, should be run every time user is trying to get some resource from app
    app.put('/check', checkIfLogged);

    //returns mock account - but in the exactly the same form as Open Banking API does
    app.get('/ob/account/:accountId', getMockAccount);

    //returns mock account balances - but in the exactly the same form as Open Banking API does
    app.get('/ob/account/:accountId/balances', getMockBalances);
};

async function checkIfLogged(req, res) {
    const token = req.body.token;
    res.send(await Token.verifyToken(token));
}

function getMockBalances(req, res) {
    const accountId = req.params;
    console.log(accountId);
    const mockBalances = {
        "account": {},
        "balances": [
            {
                "balanceAmount": {
                    "currency": "EUR",
                    "amount": 123
                },
                "balanceType": "closingBooked",
                "lastChangeDateTime": "2018-10-12T20:32:25.357Z",
                "referenceDate": "2018-10-12",
                "lastCommittedTransaction": "string"
            }
        ]
    }
    res.send(mockBalances);
}

function getMockAccount(req, res) {
    const accountId = req.params;
    console.log(accountId);
    const mockAccounts = {
        accounts: [
            {
                description: "Account Details for a regular Account",
                value: {
                    account: {
                        resourceId: "3dc3d5b3-7023-4848-9853-f5400a64e80f",
                        iban: "FR7612345987650123456789014",
                        currency: "EUR",
                        product: "Girokonto",
                        cashAccountType: "CurrentAccount",
                        name: "Main Account",
                        _links: {
                            balances: {
                                href: "/v1/accounts/3dc3d5b3-7023-4848-9853-f5400a64e80f/balances"
                            },
                            transactions: {
                                href: "/v1/accounts/3dc3d5b3-7023-4848-9853-f5400a64e80f/transactions"
                            }
                        }
                    }
                }
            }
        ]
    };
    res.send(mockAccounts);
}

async function getAccounts(req, res) {
    accountList = await db.getAccounts();
    res.send(accountList)
}

function hash(req, res) {
    console.log(req.body);
    const password = req.body.password;
    console.log(password);
    const hashedPassword = db.hash(password);
    res.send(hashedPassword);
}

async function getAccount(req, res) {
    const id = req.params.id;
    account = await db.getAccount(id);
    res.send(account);
}

//If the logging in end with success, the token is send back with code 200, if user doesnt exist, just the 666 code, if the password is wrong, just the 665 code.
//TODO checking does the user exist may not me required, authentication only would also work fine - depends what would we need on front
async function login(req, res) {
    const account = req.body;
    console.log(account);
    if (await db.checkIfAccountExists(account.id)) {
        if (await db.authenticate(account)) {
            const token = Token.createToken({account: account.name});
            res.send(token);
        } else {
            res.status(401).send();
        }
    } else {
        res.status(403).send();
    }
}