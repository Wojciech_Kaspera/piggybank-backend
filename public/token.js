const JWT = require('jsonwebtoken');
const secret = 'whatever';
const config = {
    algorithm: 'HS256',
    expireTime: 300
};
module.exports = {
    createToken(payload) {
        return JWT.sign(payload, secret, {algorithm: config.algorithm, expiresIn: config.expireTime});
    },
    verifyToken(token) {
        if (token) {
            return(JWT.verify(token, secret, (err) => {
                if (err) {
                    console.log(err);
                    return false;
                } else {
                    return true;
                }
            }));
        } else {
            console.log('token not provided');
        }
    }
};
