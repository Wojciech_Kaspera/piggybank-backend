const bcrypt = require('bcrypt-nodejs');

module.exports = {
    hashPassword(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    },

    validPassword(password, hashedPassword) {
        return bcrypt.compareSync(password, hashedPassword);
    }
};

