const firebase = require('firebase');
const userFunctions = require('./user');

let dbAccountsRef;

module.exports = {
    initialize() {
        firebase.initializeApp(this.config);
        dbAccountsRef = firebase.database().ref('accounts');
    },
    config: {
        apiKey: "AIzaSyBR-j_2Lp3GWTATGQpMvaNpPalLoWPCz4Y",
        authDomain: "piggybank-2d100.firebaseapp.com",
        databaseURL: "https://piggybank-2d100.firebaseio.com",
        projectId: "piggybank-2d100",
        storageBucket: "",
        messagingSenderId: "56858490723"
    },
    getAccounts() {
        return new Promise((resolve, reject) => {
            dbAccountsRef.once('value', snapshot => {
                resolve(snapshot.val());
            });
        });
    },
    getAccount(accountId) {
        return new Promise((resolve, reject) => {
            dbAccountsRef.once('value', snapshot => {
                snapshot.val().map(account => {
                    if (account.id == accountId) {
                        resolve(account);
                    }
                });
                resolve('No account with such ID');
            });
        });
    },
    checkIfAccountExists(accountId) {
        return new Promise(resolve => {
            dbAccountsRef.once('value', snapshot => {
                snapshot.val().map(account => {
                    if (account.id == accountId) {
                        resolve(true);
                    }
                });
                resolve(false);
            });
        });
    },
    hash(password) {
        return userFunctions.hashPassword(password);
    },
    authenticate(validatedAccount) {
        let result;
        return new Promise(resolve => {
            dbAccountsRef.once('value', snapshot => {
                const accounts = snapshot.val();
                let hashedPassword;
                accounts.map(theAccount => {
                    if (theAccount.id == validatedAccount.id) {
                        hashedPassword = theAccount.password;
                    }
                });
                result = userFunctions.validPassword(validatedAccount.password, hashedPassword);
                console.log(result);
                resolve(result)
            });
        });
    }
};